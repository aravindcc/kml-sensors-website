import React, { useState, useEffect } from "react";
import { Parallax, ParallaxLayer } from "react-spring/renderprops-addons";
import "./App.css";
import KLHeader from "./elements/KLHeader";
import KLHome from "./elements/KLHome";
import KLMenu from "./elements/KLMenu";
import KLFiller from "./elements/KLFiller";
import KLInsert from "./elements/KLInsert";
import KLDetailProduct from "./elements/KLDetailProduct";
import KLApplications from "./elements/KLApplications";
import KLAbout from "./elements/KLAbout";
import KLContact from "./elements/KLContact";
import { KLPages } from "./elements/KLMenu";

function App() {
  const [showingInsert, setShowingInsert] = useState(false);
  const [showFixed, setShowFixed] = useState(false);
  const [currentPage, setCurrentPage] = useState(KLPages.home);
  const [parallax] = useState(React.createRef());

  const scrollToPage = page => {
    if (parallax.current == null) return;
    var scrollLoc;
    if (page == KLPages.home) {
      scrollLoc = 0;
    } else if (page == KLPages.products) {
      scrollLoc = 0.6;
    } else if (page == KLPages.applications) {
      scrollLoc = 1.3;
    } else if (page == KLPages.about) {
      scrollLoc = 1.9;
    } else {
      scrollLoc = 2.5;
    }
    parallax.current.scrollTo(scrollLoc);
  };
  useEffect(() => {
    if (parallax.current == null) return;
    const fixHeader = (() => {
      let scrollTop = parallax.current.current;
      var currentPage;
      if (scrollTop < 200) {
        currentPage = KLPages.home;
      } else if (scrollTop < 1000) {
        currentPage = KLPages.products;
      } else if (scrollTop < 1500) {
        currentPage = KLPages.applications;
      } else if (scrollTop < 1800) {
        currentPage = KLPages.about;
      } else {
        currentPage = KLPages.contact;
      }

      setShowFixed(scrollTop >= 100);
      setShowingInsert(scrollTop > 800);
      setCurrentPage(currentPage);
    }).bind(KLPages);
    var scrollingInterval = setInterval(fixHeader, 50);
    return () => {
      clearInterval(scrollingInterval);
    };
  }, []);

  return (
    <>
      {showFixed ? (
        <KLMenu fixed currentPage={currentPage} scrollToPage={scrollToPage} />
      ) : (
        true
      )}
      <Parallax pages={3.8} ref={parallax} offset={2}>
        <ParallaxLayer
          offset={0}
          speed={-0.1}
          style={{ zIndex: 100, pointerEvents: "none" }}
        >
          <KLHeader />
          {!showFixed ? (
            <KLMenu currentPage={currentPage} scrollToPage={scrollToPage} />
          ) : (
            true
          )}
        </ParallaxLayer>
        <ParallaxLayer offset={0.25} speed={0.5}>
          <KLHome />
        </ParallaxLayer>
        <ParallaxLayer offset={0.4} speed={1} style={{ pointerEvents: "none" }}>
          <KLFiller />
        </ParallaxLayer>
        <ParallaxLayer offset={0.8} speed={0.5}>
          <KLDetailProduct />
        </ParallaxLayer>
        <ParallaxLayer offset={1.5} speed={0.1}>
          <KLInsert isShown={showingInsert} />
        </ParallaxLayer>
        <ParallaxLayer offset={1.6} speed={0.5}>
          <KLApplications />
        </ParallaxLayer>
        <ParallaxLayer offset={2} speed={0.5}>
          <KLAbout />
        </ParallaxLayer>
        <ParallaxLayer offset={2.8} speed={0.5}>
          <KLContact />
        </ParallaxLayer>
      </Parallax>
    </>
  );
}

export default App;
