import React from "react";
import bg from "../assets/Slow-Background@3x.png";

function KLFiller() {
  return <img src={bg} style={{ width: "100%" }} />;
}

export default KLFiller;
