import React from "react";
import "./css/Products.css";

function ProductElement({ image, title }) {
  return (
    <div className="KLProductElement">
      <img src={image} />
      <a>{title}</a>
    </div>
  );
}

function KLProducts({ data }) {
  var productify = i => {
    var item = data[i];
    return <ProductElement key={i} {...item} />;
  };

  return (
    <div className="KLProductContainer">
      {Object.keys(data).map(productify)}
    </div>
  );
}

export default KLProducts;
