import React from "react";

import "./css/Applications.css";

import wind from "../assets/wind.svg";
import automotive from "../assets/automotive.svg";
import aerospace from "../assets/aerospace.svg";
import defence from "../assets/defence.svg";
import railway from "../assets/railway.svg";
import welding from "../assets/welding.svg";
import energy from "../assets/energy.svg";
import pipe from "../assets/pipe.svg";

function KLApplications() {
  const applications = [
    { vector: wind, title: "wind towers" },
    { vector: automotive, title: "automotive" },
    { vector: aerospace, title: "aerospace" },
    { vector: defence, title: "defence" },
    { vector: railway, title: "railways" },
    { vector: welding, title: "robot welding" },
    { vector: energy, title: "energy" },
    { vector: pipe, title: "tubes & pipes" }
  ];
  const gridColors = ["gridBlue", "gridGreen", "gridYellow"];
  return (
    <div className="applicationsContainer">
      <h1>Applications</h1>
      <div className="applicationGrid">
        {applications.map((item, i) => (
          <div className={gridColors[i % gridColors.length]}>
            <a>{item.title}</a>
            <img src={item.vector} />
          </div>
        ))}
      </div>
    </div>
  );
}

export default KLApplications;
