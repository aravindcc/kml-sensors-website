import React, { useState, useEffect } from "react";
import "./css/Menu.css";

export const KLPages = {
  home: "home",
  products: "products",
  applications: "applications",
  about: "about us",
  contact: "contact us"
};

function KLMenu({ fixed, currentPage, scrollToPage }) {
  return (
    <div className={"headerMenu" + (fixed ? " fixed" : "")}>
      <ul>
        {Object.keys(KLPages).map(key => (
          <li>
            <a
              className={KLPages[key] == currentPage ? "selected" : ""}
              onClick={() => scrollToPage(KLPages[key])}
            >
              {KLPages[key]}
            </a>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default KLMenu;
