import React from "react";
import "./css/Header.css";
import logo from "../assets/Logo@3x.png";

function KLHeader() {
  return (
    <div className="header">
      <img className="headerLogo" src={logo} />
      <h1 className="headerTagline">
        India's
        <a>
          No. <b>1</b>
        </a>
        laser sensor company
      </h1>
    </div>
  );
}

export default KLHeader;
