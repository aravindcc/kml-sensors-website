import React from "react";
import { CarouselProvider, Slider, Slide, DotGroup } from "pure-react-carousel";
import "./css/Home.css";
import "pure-react-carousel/dist/react-carousel.es.css";

import sample from "../assets/pngs/sample.jpg";

function KLHome() {
  return (
    <div className="homeContainer">
      <CarouselProvider
        naturalSlideWidth={100}
        naturalSlideHeight={125}
        totalSlides={3}
      >
        <div className="homeCarousel">
          <Slider
            className="homeCarouselSlider"
            classNameAnimation="homeCarouselSliderTrans"
          >
            <Slide index={0}>
              <div className="homeCarouselSlide">
                <p>
                  We create a wide a range of Laser and Vision Sensors for
                  Industrial Inspection, Measurements and Control systems. Lorem
                  ipsum dolor sit amet, consectetur adipiscing elit, sed do
                  eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>
                <img src={sample} />
              </div>
            </Slide>
            <Slide index={1}>
              <div className="homeCarouselSlide">
                <p>
                  We create a wide a range of Laser and Vision Sensors for
                  Industrial Inspection, Measurements and Control systems. Lorem
                  ipsum dolor sit amet, consectetur adipiscing elit, sed do
                  eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>
                <img src={sample} />
              </div>
            </Slide>
            <Slide index={2}>
              <div className="homeCarouselSlide">
                <p>
                  We create a wide a range of Laser and Vision Sensors for
                  Industrial Inspection, Measurements and Control systems. Lorem
                  ipsum dolor sit amet, consectetur adipiscing elit, sed do
                  eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>
                <img src={sample} />
              </div>
            </Slide>
          </Slider>
          <DotGroup className="homeCarouselPager" />
        </div>
      </CarouselProvider>
    </div>
  );
}

export default KLHome;
