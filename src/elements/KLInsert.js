import React from "react";
import right from "../assets/right-insert@3x.png";
import left from "../assets/left-insert@3x.png";
import { useSpring, animated } from "react-spring";
import "./css/Insert.css";

function KLInsert({ isShown }) {
  var moveBy = 800;
  const propsLeft = useSpring({
    transform: !isShown
      ? `translate3d(-${moveBy}px,0,0)`
      : "translate3d(0px,0,0)",
    from: { transform: `translate3d(-${moveBy}px,0,0)` }
  });
  const propsRight = useSpring({
    transform: !isShown
      ? `translate3d(${moveBy}px,0,0)`
      : "translate3d(0px,0,0)",
    from: { transform: `translate3d(${moveBy}px,0,0)` }
  });
  return (
    <div className="insertContianer">
      <animated.img src={left} className="insertLeft" style={propsLeft} />
      <animated.img src={right} className="insertRight" style={propsRight} />
    </div>
  );
}

export default KLInsert;
