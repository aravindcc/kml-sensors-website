import React from "react";
import "./css/About.css";
import laserline from "../assets/laserline@3x.png";

import india from "../assets/india.png";
import cost from "../assets/cost.svg";
import support from "../assets/support.svg";
import innovative from "../assets/innovative.svg";

function KLAbout() {
  const reasons = [
    { vector: india, title: "made in India" },
    { vector: cost, title: "quick support" },
    { vector: support, title: "cost effective" },
    { vector: innovative, title: "innovative" }
  ];
  return (
    <div className="aboutContainer">
      <h1>About Us</h1>
      <img src={laserline} className="aboutLaserLine" />
      <p>
        <b>KML Sensors</b> is part of Kalaimakal group of companies.{" "}
        <b>Kalaimakal Group</b> head quartered in Krishnagiri, Tamil Nadu, India
        was established in 1979 and named after the Hindu goddess Saraswati
        (Goddess of Knowledge, Music, arts and Science).
        <br />
        <br />
        <b>Kalaimakal Systems</b> was established in 2012 to provide latest
        technologies in 3D Laser Vision and Control System automation for
        welding and related applications. Kalaimakal Systems proudly launched
        under the Brand name “KML Sensors” during IMTEX 2017 Bangalore. Lorem
        ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit
        esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
        cupidatat non proident, sunt in culpa qui officia deserunt mollit anim
        id est laborum.
        <br />
        <br />
        <b>Why we think you should pick us: </b>
        <div className="aboutReasonsContainer">
          {reasons.map(item => (
            <div>
              <div>
                <img src={item.vector} />
              </div>
              <a>{item.title}</a>
            </div>
          ))}
        </div>
      </p>
    </div>
  );
}

export default KLAbout;
