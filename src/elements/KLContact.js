import React from "react";
import "./css/Contact.css";
import happyClients from "../assets/HappyClients.png";
import { message } from "antd";

function KLContact() {
  const onClick = () => {
    // send of form but for now
    if (Math.random() >= 0.5) {
      message.success("Message has been sent!");
    } else {
      message.warning("There was a problem sending the message.");
    }
  };
  return (
    <div className="contactContainer">
      <h1>Contact Us</h1>
      <p>
        Feel like we can help? Drop us a message in the form and we’ll be happy
        to assist you.{" "}
      </p>
      <div className="contactForm">
        <div className="contactLeft">
          <div className="styled-input">
            <input type="text" required />
            <label>Name</label>
          </div>
          <div className="styled-input">
            <input type="text" required />
            <label>Email</label>
          </div>
          <div className="styled-input">
            <input type="text" required />
            <label>Phone Number</label>
          </div>
          <div class="submit-btn" onClick={onClick}>
            submit
          </div>
        </div>
        <div className="contactRight">
          <div className="styled-input message">
            <textarea required></textarea>
            <label>Message</label>
          </div>
        </div>
      </div>
      <a>Some of our happy clients: </a>
      <img src={happyClients} />
    </div>
  );
}

export default KLContact;
