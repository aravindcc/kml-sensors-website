import React, { useState } from "react";
import "./css/DetailProduct.css";
import sample from "../assets/pngs/sample.jpg";

function KLDetailProduct() {
  const [active, setActive] = useState(0);

  const tabs = [
    {
      title: "Laser Sensors",
      body: title => {
        return (
          <div>
            <h1>{title}</h1> <b>Lorem ipsum dolor sit amet</b>, consectetur
            adipiscing Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            Phasellus nec neque nisi, dictum aliquet lectus.Lorem ipsum dolor
            sit amet, consectetur adipiscing elit. Phasellus nec neque nisi,
            dictum aliquet lectus.Lorem ipsum dolor sit amet, consectetur
            adipiscing elit. Phasellus nec neque nisi, dictum aliquet
            lectus.Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            Phasellus nec neque nisi, dictum aliquet lectus.Lorem ipsum dolor
            sit amet, consectetur adipiscing elit. Phasellus nec neque nisi,
            dictum aliquet lectus.Lorem ipsum dolor sit amet, consectetur
            adipiscing elit. Phasellus nec neque nisi, dictum aliquet
            lectus.Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            Phasellus nec neque nisi, dictum aliquet lectus.Lorem ipsum dolor
            sit amet, consectetur adipiscing elit. Phasellus nec neque nisi,
            dictum aliquet lectus. elit. Phasellus nec neque nisi, dictum
            aliquet lectus.
          </div>
        );
      }
    },
    { title: "Pipe Measurement System", body: title => <div /> },
    { title: "Laser Seam Finding System", body: title => <div /> },
    { title: "Inspection System", body: title => <div /> },
    { title: "Welding Cameras", body: title => <div /> },
    { title: "Vision System", body: title => <div /> }
  ];
  return (
    <div className="productsContainer">
      <img src={sample} />
      <div className="detailedView">
        <h1>Products</h1>
        <div className="detailedProductContainer">
          <div class="detail-tabs">
            <div class="detail-tabs-nav">
              {tabs.map((item, i) => (
                <div className={active == i ? "detail-tab-active" : ""}>
                  <a onClick={() => setActive(i)}>{item.title}</a>
                </div>
              ))}
            </div>
            <div class="detail-tabs-stage">
              {tabs.map((item, i) => {
                return active == i ? (
                  <div id={"tab-" + i}>
                    <p>{item.body(item.title)}</p>
                  </div>
                ) : (
                  true
                );
              })}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default KLDetailProduct;
